# SAPUI5 express middleware

This project is forked from https://github.com/ThePlenkov/express-fiori-ws
I've added sap-client and user support.

This middleware plugin is created for fast UI5 development and testing

This is a node.js implementation of path routing based on neo-app.json

Also it provides sandbox Fiori launchpad with modified resource path for loading UI5 resources from CDN.


