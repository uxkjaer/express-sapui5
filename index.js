"use strict";

const proxy = require("http-proxy-middleware");
const fetch = require("node-fetch");
const cheerio = require("cheerio");
const noCache = require("nocache");
const path      = require('path');

var im = require('istanbul-middleware'),
    isCoverageEnabled = (process.env.COVERAGE == "true"); // or a mechanism of your choice

//before your code is require()-ed, hook the loader for coverage
if (isCoverageEnabled) {
    console.log('Hook loader for coverage - ensure this is not production!');
    im.hookLoader(__dirname);
        // cover all files except under node_modules
        // see API for other options
}


    
// set up basic middleware
// ...


//add your router and other endpoints
//...

let serveUi5 = function(oSettings, app) {
  var oNeoApp = oSettings.neoApp,
    oDestinations = oSettings.destinations,
    oManifest = oSettings.manifest,
    oAgent = oSettings.agent;

  let cdn = oSettings.cdn || "https://ui5.sap.com";
  if (oSettings.version) {
    cdn += "/" + oSettings.version;
  }

  const homePage =
    "/test-resources/sap/ushell/shells/sandbox/fioriSandbox.html";
  // redirect to FLP
  app.get("/", async (req, res) => {
    res.redirect(homePage);
  });

  // redirect to FLP
  app.get(homePage, async (req, res) => {
    let flp = await fetch(cdn + homePage, {
      agent: oAgent
    });
    const $ = cheerio.load(await flp.text());
    console.log(cdn);
    $("#sap-ui-bootstrap").attr().src = cdn + "/resources/sap-ui-core.js";
    $("#sap-ushell-bootstrap").attr().src =
      cdn + "/test-resources/sap/ushell/bootstrap/sandbox.js";

    res.send($.html());
  });

  // no odata cache (including metadata)
  app.use("/sap/opu", noCache());

 
// add the coverage handler
if (isCoverageEnabled) {
  //enable coverage endpoints under /coverage
  app.use('/coverage', im.createHandler());
}

 


  if (oNeoApp && oNeoApp.routes) {
    oNeoApp.routes.forEach(function(oRoute) {
      var oTarget = oRoute.target;
      if (oTarget && oTarget.name !== "local") {
        // proxy options
        var oOptions = {};

        // search for destination
        if (oDestinations && oTarget.name) {
          var oDestination = oDestinations[oTarget.name];
          if (oDestination) {
            oOptions.target = oDestination.target;
            oOptions.changeOrigin = true;
            oOptions.secure = false;
            if (oDestination.useProxy) {
              oOptions.agent = oAgent;
            }

            //	Added support for client and basic authentication
            if (oDestination.sapclient) {
              app.use("/sap/opu", function(req, res, next) {
                
                if(req.method == "GET" && req.originalUrl.includes("$metadata")) {
                  console.log("Adding params");
                  req.query = req.query +"&sap-client=" +oDestination.sapclient;
                  req.originalUrl = req.originalUrl +"&sap-client=" +oDestination.sapclient;
                  console.log(req.originalUrl)
                }
                next()
  
              });
              
              
            }
            
            if (oDestination.Authentication === "Basic") {
              oOptions.headers = {
                "Authorization": "Basic " + Buffer.from(oDestination.User +
                  ":" + oDestination.Password).toString("base64")
              };
            }
            let sVersion = oSettings.version || oTarget.version;

            if (oTarget.name === "sapui5" && sVersion) {
              oOptions.target = oOptions.target + "/" + sVersion;
            }
          }
        }

        if (oRoute.path && oTarget.entryPath) {
          var oRouteNew = {};
          var sPathOld = "^" + oRoute.path;
            oRouteNew[sPathOld] = oTarget.entryPath;
            oOptions.pathRewrite = oRouteNew;
          
         
        }
      
        console.log("Path is " +oRoute.path);
        console.log("Options are :" +oOptions)
        app.use(oRoute.path, proxy(oOptions));
      }

      else {
        //File is being served from localhost
        let oDestination = oDestinations[oTarget.name];
        if (oDestination) {
          //Create a relative mapping rule for localhost
          let sPath = "*" +oRoute.path +"/*";
          
          app.use(sPath, function(req, res, next){
            // Split the url so we can fetch the UI5 component
            let length = req.originalUrl.split(oRoute.path).length
            let dirLib = req.originalUrl.split(oRoute.path);
            let dirLibSplit = dirLib[length - 1].split("/");
          
            let webappDir = oDestination.target;

            //Build the local file string, but remember to inject the webapp folder
            for (let i = 0; i < dirLibSplit.length; i++){
              if (i === 2){
                webappDir = webappDir + "/webapp/" +dirLibSplit[i]
              }
              else if (i === 1 || i > 2) {
                webappDir = webappDir + "/" +dirLibSplit[i]
              }
               
            }
            //Return the content from local filesystem
            res.sendFile(path.normalize(__dirname + webappDir))
          });
        }
      }
    });
  }

  return app;
};

let bInitialized;

module.exports = oSettings => (req, res, next) => {
  if (!bInitialized) {
    bInitialized = true;
    serveUi5(oSettings, req.app);
  }
  next();
};
